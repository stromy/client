import { Injectable, OnDestroy } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
// import { share } from 'rxjs/operators';
// import { AddTreeModule } from '../add-tree.module';
import { TreeI, Tree, ClassificationSchema, ClassificationInterface } from '../lib';
import { environment } from '../../environments/environment';
import { map } from 'rxjs/operators';

@Injectable()

export class AddTreeService implements OnDestroy {

  /** class keeping all informations about tree user is editing */
  public T: TreeI = new Tree();

  /** matrix of classification elements user filled in */
  public kData: number[][] = [];

  /** structure of object holding texts in configuration file */
  public ConfKData: ClassificationInterface[];

  /** message returned from server */
  public message: any;

  /** stored image files apploaded by user */
  public imageFiles: File[] = [];

  /** maximum number of images */
  public maxImageN = 3;

  /** stored text files apploaded by user */
  public textFiles: File[] = [];

  constructor(private http: HttpClient) {
    this.ConfKData = ClassificationSchema;
    for (let k = 0; k < ClassificationSchema.length; k++) {
      const kRow = [];
      for (let subK = 0; subK < ClassificationSchema[k].I; subK++) {
        kRow.push(0);
      }
      this.kData.push(kRow);
    }
  }

  /** send form to backend and return observable of response */
  send(): Observable<any> {
    this.prepareData();
    const body = this.allTogether();
    // console.log('SENDING BODY:', body);

    const observable = new Observable(observer => {
      this.http
        .post(
          `${environment.server}/tree/addTree`,
          body,
          {
            headers: new HttpHeaders().set('Content-Type', 'application/json'),
          }).subscribe((data: any) => {
            if (data.status !== 'ok') {
              // console.log("status message")
              observer.next(data);
              observer.complete();

            } else {  // status OK, send image files
              const id = data.message;

              if (this.imageFiles && this.imageFiles.length > 0) {
                const fd = this.prepareFiles(this.imageFiles);
                fd.append('id', id);
                this.http.post(`${environment.server}/tree/addTreePictures`, fd)
                  .subscribe(imageData => {
                    observer.next(imageData);
                    observer.complete();
                  });
              } else {
                observer.next(data);
                observer.complete();
              }
            }
          });
    });
    return observable;
  }

  prepareFiles(files: File[]): FormData {
    const fd = new FormData();
    for (const file of files) {
      fd.append('photos', file, file.name);
    }
    return fd;
  }

  /** form body before sending request to backend */
  allTogether(): string {
    const jsonBody = `
    {
      "strom":${JSON.stringify(this.T.S)},
      "lokal":${JSON.stringify(this.T.L)},
      "kateg":${JSON.stringify(this.T.K)},
      "pisemneD":${JSON.stringify(this.T.PD)},
      "obrazoveD":${JSON.stringify(this.T.OD)},
      "comment":${JSON.stringify(this.T.C)},
      "ohro":${JSON.stringify(this.T.O)}
    }`;
    return jsonBody;
  }

  /** prepare data before forming body
   * classification matrix on strings and some hardcoded fields for now
   */
  prepareData = () => {
    for (let i = 0; i < this.kData.length; i++) {
      let row = `${this.kData[i][0]}`;

      for (let j = 1; j < this.kData[i].length; j++) {
        row += `,${this.kData[i][j]}`;
      }
      this.T.K[`KATEG${i + 1}`] = row;
    }
    if (this.T.S.DATIN instanceof Date) {
      this.T.S.DATIN = `${this.T.S.DATIN.getFullYear()}.${this.T.S.DATIN.getMonth()}.${this.T.S.DATIN.getDate()}`;
    }

    // TODO this should be filled by form
    this.T.S.VLAST = 'APPVS';
  }

  ngOnDestroy() { }
}
