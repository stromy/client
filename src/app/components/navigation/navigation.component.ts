import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { HeaderService } from 'src/app/services/header.service';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent implements OnInit, OnDestroy {

  header: string;
  headerSubscription: Subscription;

  constructor(private headerß: HeaderService) { }

  ngOnInit() {
    this.headerSubscription = this.headerß.headerSubject.subscribe(header => {
      this.header = header;
    });
  }

  ngOnDestroy() {
    if (this.headerSubscription) {this.headerSubscription.unsubscribe()};
  }

}
