import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AddTreeService } from '../../services/add-tree.service';
import { ClassificationInterface } from '../../lib';
import { MatSnackBar } from '@angular/material/snack-bar';
import { HeaderService } from 'src/app/services/header.service';
import { ChangeDetectionStrategy } from '@angular/compiler/src/compiler_facade_interface';

@Component({
  selector: 'app-classification',
  templateUrl: './classification.component.html',
  styleUrls: ['./classification.component.scss']
})

export class ClassificationComponent implements OnInit {
  kategorie: number;
  katData: string[];
  configData: ClassificationInterface;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private addTreeService: AddTreeService,
    private snackBar: MatSnackBar,
    private headerß: HeaderService,
    private cdr: ChangeDetectorRef
    ) {
  }

  ngOnInit() {
    this.route.params.subscribe((params) => {
      this.kategorie = params.kategorie;
      this.configData = this.addTreeService.ConfKData[this.kategorie];
      this.headerß.setHeader(`KATEGORIE ${+this.kategorie + 1}`);
      this.cdr.detectChanges()
    });
  }

  /** submit and send tree */
  submit() {
    this.addTreeService.send()
      .subscribe((data: any) => {
        // console.log(data);
        if (data.status === 'ok') {
          this.snackBar.open('strom byl odeslán', null, {duration: 3000});
          this.router.navigate(['/']);
        } else {
          alert(`Vyskytla se chyba!, zpráva je: ${data.message}`);
        }
      });
  }
}
