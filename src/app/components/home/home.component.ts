import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { TreeMinimum } from '../../lib/interfaces/tree.interface';
import { HeaderService } from 'src/app/services/header.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})

export class HomeComponent implements OnInit {
  trees: TreeMinimum[] = [];
  defaultRadius = 1000;

  constructor(
    private http: HttpClient,
    private cdr: ChangeDetectorRef,
    private headerß: HeaderService
    ) {
  }

  ngOnInit() {
    this.loadTreasAround();
    this.headerß.setHeader('VÝZNAMNÉ STROMY');
  }

  /** get trees around users current location */
  loadTreasAround() {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition((position) => {
        const center: [number, number] = [position.coords.longitude, position.coords.latitude];
        this.treesRequest(center);
      });
    }
  }

  /** get trees around the center (usually around user location)
   * @param center coordinates, usually users location
   */
  treesRequest(center: [number, number]) {
    const body = `center=${JSON.stringify(center)}&radius=${this.defaultRadius}`;
    return this.http
      .post(`${environment.server}/tree/getTreesAround`,
      body,
      {
        headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
      })
      .subscribe((trees: TreeMinimum[]) => {
        if(trees && trees.length >= 0) {
          trees.sort((a, b) => {
            return a.distance - b.distance;
          });
        } 
        this.trees = trees;
        this.cdr.detectChanges();
      });
  }

}
