import { Component, Input, OnInit } from '@angular/core';
import { environment } from '../../../../environments/environment';

@Component ({
  selector: 'app-tree-tile',
  templateUrl: './tree-tile.component.html',
  styleUrls: ['./tree-tile.component.scss']
})

export class TreeTileComponent implements OnInit{
  @Input() treeName: string;
  @Input() distance: number;
  @Input() id: string;
  @Input() type: string;
  @Input() validated: string;
  @Input() od: string;

  images: string[];
  constructor() {}

  ngOnInit() {
    if (this.od && this.od !== "") {

      this.images = this.od.split(",")
      for(let i in this.images) {
        // localhost:3000/tree/uploads/obrazove/3_2021-03-02_(0).jpg
        this.images[i] = `${environment.server}/tree/uploads/obrazove/${this.images[i]}`
      }
    }
    // console.log(this.treeName)
  }
}
