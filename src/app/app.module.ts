import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './materials.module';
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';

// components
import { NavigationComponent } from './components/navigation/navigation.component';
import { InfoDialogComponent } from './components/info-dialog/info-dialog.component';
import { HomeComponent } from './components/home/home.component';
import { TreeTileComponent } from './components/home/tree-tile/tree-tile.component';
import { ClassificationComponent } from './components/classification/classification.component';
import { BtnGroupComponent } from './components/classification/btn-group/btn-group.component';
import { ParamsComponent } from './components/params/params.component';

// services
import { AddTreeService } from './services/add-tree.service';
import { HeaderService } from './services/header.service';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';

@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent,
    InfoDialogComponent,
    HomeComponent,
    TreeTileComponent,
    ClassificationComponent,
    BtnGroupComponent,
    ParamsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MaterialModule,
    HttpClientModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    FormsModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production })
  ],
  providers: [AddTreeService, HeaderService],
  bootstrap: [AppComponent],
  entryComponents: [InfoDialogComponent]
})
export class AppModule { }
