import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './components/home/home.component';
import { ParamsComponent } from './components/params/params.component';
import { ClassificationComponent } from './components/classification/classification.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'parameters', component: ParamsComponent },
  { path: 'kategorie/:kategorie', component: ClassificationComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
