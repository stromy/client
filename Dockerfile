# BUILD
FROM node:14.15.3 as node

WORKDIR /app

COPY . .

RUN npm install

# RUN npm audit fix

RUN npm run build --prod

# DEPLOY

FROM nginx:alpine

COPY ./nginx.conf /etc/nginx/conf.d/

RUN rm -rf /usr/share/nginx/html/*

COPY --from=node /app/dist /usr/share/nginx/html
